﻿using ProductRecommendationCore;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encog.Engine.Network.Activation;

namespace ProductRecommendationConsole
{
  class Program
  {
    static void Main(string[] args)
    {
      var method = args[0];
      var trainFile = "eval_current_month_dataset.csv";
      var testFile = "test_ver2.csv";
      var logFile = "experimentLog.csv";



      //method,iterations,input,rows,time,train error, test error,score
      File.WriteAllText(logFile, "method,iterations,input_rows,time,train_error,test_error,xvalidation,decay_multiplier\n");

      var convertedTrainFile = "converted_" + trainFile;
      var columnsToRemove = new[] { "fecha_alta", "ncodpers", "buy_class", "n_new_products", "new_products", "fecha_dato" };

      var rowCounts = new int[] {5000,10000, 100000 };

      Console.WriteLine("Starting...");
      foreach (var rowCount in rowCounts)
      {
        var util = new CsvUtility(trainFile, convertedTrainFile);
        util.ConvertData(rowCount, columnsToRemove, false);
        var anwers = util.answers;

        var convertedTestFile = "converted_" + testFile;

        if (method == "mlp")
        {

          var xvalidations = new[] { 20, 30, 50 };
          foreach (var xvalidate in xvalidations)
          {
            Console.WriteLine("method: {0}\trows: {1}\txvalidation: {2}", method, rowCount, xvalidate);

            int iterations;
            var mlpNetwork = new MLPBuilder(convertedTrainFile, xvalidate);
            var start = DateTime.Now;
            var mlpTrainingError = mlpNetwork.Train(out iterations);
            var mlpTestError = mlpNetwork.Test();
            var elapsed = DateTime.Now - start;
            Console.WriteLine("MLP training error: {0}, test error: {1}", mlpTrainingError, mlpTestError);

            string outputFile = String.Format("mlp_{0}rows_{1}iterations_{2}crossvalidate", rowCount, iterations,
              xvalidate);
            mlpNetwork.ComputeResults(convertedTestFile, outputFile, true);
            File.AppendAllText(logFile, String.Format("mlp,{0},{1},{2},{3},{4},{5}\n", iterations, rowCount, elapsed.TotalSeconds, mlpTrainingError, mlpTestError, xvalidate));
          }

        }
        else if (method == "som")
        {
          int maxIterations = Math.Max(10, 250 - (rowCount / 500 - 1) * 100);
          // ReSharper disable once LoopVariableIsNeverChangedInsideLoop
          for (double decayMultiplier = 0.5; decayMultiplier <= 2.0; decayMultiplier += 0.5)
          {
            Console.WriteLine("method: {0}\trows: {1}\tdecay_multiplier: {2}", method, rowCount, decayMultiplier);

            var SOMNetwork = new SOMBuilder(convertedTrainFile, anwers, maxIterations);
            Console.WriteLine("Start training...");
            var start = DateTime.Now;
            var somTrainingError = SOMNetwork.Train(decayMultiplier);

            var elapsedTime = DateTime.Now - start;
            Console.WriteLine("Training finished in {0}s", elapsedTime.Seconds);
            Console.WriteLine("SOM training error: {0}", somTrainingError);
            Console.WriteLine("Compute results?(y/N): ");
            string outputfile = String.Format("som_{0}rows_{1}iterations_{2}decay", rowCount, maxIterations,
              decayMultiplier);
            SOMNetwork.ComputeResults(convertedTestFile, outputfile, true);
            File.AppendAllText(logFile, String.Format("som,{0},{1},{2},{3},{4},{5},{6}\n", maxIterations, rowCount,
              elapsedTime.TotalSeconds, somTrainingError, "NA", "NA", decayMultiplier));

          }
        }
        else if (method == "custom_mlp")
        {
          int maxIterations;
          bool bias = method == "custom_mlp";

          Console.WriteLine("method: {0}\trows: {1}\t", method, rowCount);
          var neuronCounts = new[] { 100, 100, 100};
          var customMlp = new CustomMlpBuilder(convertedTrainFile, bias, neuronCounts, new ActivationSigmoid());
          Console.WriteLine("Start training...");
          var start = DateTime.Now;
          var somTrainingError = customMlp.Train(out maxIterations);

          var elapsedTime = DateTime.Now - start;
          Console.WriteLine("Training finished in {0}s", elapsedTime.Seconds);
          Console.WriteLine("SOM training error: {0}", somTrainingError);
          Console.WriteLine("Compute results?(y/N): ");
          string outputfile = String.Format("custom_mlp_{0}rows{1}", rowCount, bias ? "_biased" : "");
          customMlp.ComputeResults(convertedTestFile, outputfile, true);
          File.AppendAllText(logFile, String.Format("som,{0},{1},{2},{3},{4},{5},{6}\n", maxIterations, rowCount,
            elapsedTime.TotalSeconds, somTrainingError, "NA", "NA", "NA"));


        }
      }
      //util = new CsvUtility(testFile, convertedTestFile);
      //columnsToRemove = new[] { "fecha_alta", "fecha_dato", "ult_fec_cli_1t", "conyuemp", "tipodom", "cod_prov", "buy_class", "n_new_products", "new_products" };
      //util.ConvertData(929616, columnsToRemove, false);



      Console.WriteLine("Press key to exit...");
      Console.ReadKey();
    }
  }
}
