﻿using Encog.ML.Data.Versatile;
using Encog.ML.Data.Versatile.Columns;
using Encog.ML.Data.Versatile.Sources;
using Encog.ML.Factory;
using Encog.ML.Model;
using Encog.Util.CSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductRecommendationCore
{
    public class NeuralNetwork
    {
        protected EncogModel _model;
        protected VersatileMLDataSet _dataSet;
        protected NormalizationHelper _normalizationHelper;
        protected int _outputColumnsCount;
      protected string[] products;

      public NeuralNetwork()
      {
        products = 
            "ind_ahor_fin_ult1 ind_aval_fin_ult1 ind_cco_fin_ult1 ind_cder_fin_ult1 ind_cno_fin_ult1 ind_ctju_fin_ult1 ind_ctma_fin_ult1 ind_ctop_fin_ult1 ind_ctpp_fin_ult1 ind_deco_fin_ult1 ind_deme_fin_ult1 ind_dela_fin_ult1 ind_ecue_fin_ult1 ind_fond_fin_ult1 ind_hip_fin_ult1 ind_plan_fin_ult1 ind_pres_fin_ult1 ind_reca_fin_ult1 ind_tjcr_fin_ult1 ind_valo_fin_ult1 ind_viv_fin_ult1 ind_nomina_ult1 ind_nom_pens_ult1 ind_recibo_ult1".Split(' ').ToArray();

      }

        public void ReadData(string fileName)
        {
            var dataSource = new CSVDataSource(fileName, true, CSVFormat.DecimalPoint);
            _dataSet = new VersatileMLDataSet(dataSource);
            
            List<ColumnDefinition> input,output;
            DefineColumns(out output, out input);
            _dataSet.Analyze();
            DefineInputsOutputs(output, input);
            _model = new EncogModel(_dataSet);
            _model.SelectMethod(_dataSet, MLMethodFactory.TypeFeedforward);
            _dataSet.Normalize();
        }

        private void DefineColumns(out List<ColumnDefinition> outputColumns, out List<ColumnDefinition> inputColumns)
        {
            outputColumns = new List<ColumnDefinition>();
            //INPUT COLUMNS
            var input = new[] {
                _dataSet.DefineSourceColumn("ind_empleado", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("pais_residencia", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("sexo", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("age", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("ind_nuevo", ColumnType.Nominal), //checks if new customer (1 - if customer registered in previous 6 months)
                _dataSet.DefineSourceColumn("antiguedad", ColumnType.Continuous), // customer seniority in months
                _dataSet.DefineSourceColumn("indrel", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("indrel_1mes", ColumnType.Nominal), // customer type at beginning beginning of month
                _dataSet.DefineSourceColumn("tiprel_1mes", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("indresi", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("indext", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("canal_entrada", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("indfall", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("nomprov", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("ind_actividad_cliente", ColumnType.Nominal),
                _dataSet.DefineSourceColumn("renta",ColumnType.Continuous),
                _dataSet.DefineSourceColumn("segmento", ColumnType.Nominal),
            };
            //OUTPUT COLUMNS
            var products = CsvUtility.productNames();
            foreach (var productName in products)
            {
                outputColumns.Add(_dataSet.DefineSourceColumn(productName, ColumnType.Continuous));
            }
            //IGNORED COLUMNS
            //   _dataSet.DefineSourceColumn("fecha_alta", ColumnType.Nominal), // date of becoming a customer ?
            //_dataSet.DefineSourceColumn("ncodpers", ColumnType.Nominal),
            //_dataSet.DefineSourceColumn("new_products", ColumnType.Nominal),
            inputColumns = input.ToList();
        }

        private void DefineInputsOutputs(List<ColumnDefinition> output, List<ColumnDefinition> input)
        {
            foreach (var column in output)
            {
                _dataSet.DefineOutput(column);
            }
            _outputColumnsCount = output.Count;
            foreach (var column in input)
            {
                _dataSet.DefineInput(column);
            }
        }

      protected void LogEpoch(int epoch)
      {
        Console.WriteLine("EPOCH: {0}", epoch);
      }
    }
}
