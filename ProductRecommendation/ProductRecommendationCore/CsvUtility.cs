﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using Encog.Util.CSV;
using System.IO;

namespace ProductRecommendationCore
{
    public class CsvUtility
    {
        private string _inputFile;
        private string _outputFile;
        public int[][] answers;

        public CsvUtility(string inputFile, string outputFile)
        {
            _inputFile = inputFile;
            _outputFile = outputFile;
        }

        public void ConvertData(int rowsToInclude, string[] columnsToRemove, bool areNewProductsInMultipleLines)
        {
            var newProducts = "new_products";
            StreamReader input = new StreamReader(_inputFile);
            ParseCSVLine parse = new ParseCSVLine(CSVFormat.DecimalPoint);
            var columns = input.ReadLine();
            var columnNames = parse.Parse(columns).ToList();
            var removeColumnIndices = columnsToRemove.Select(column => columnNames.IndexOf(column)).Where(ind => ind >= 0).OrderByDescending(_ => _);
            var newProductsIndex = columnNames.IndexOf(newProducts);
            
            StreamWriter file = new StreamWriter(_outputFile);
            columnNames.AddRange(productNames());
            file.WriteLine(string.Join(",", columnNames.Except(columnsToRemove)));
            var answersList = new List<int[]>();
            int numberOfNewProducts = 0;
            for (int i = 0; i < rowsToInclude; i++)
            {
                if (input.EndOfStream) break;
                if(areNewProductsInMultipleLines && --numberOfNewProducts > 0)
                {
                    input.ReadLine();
                    continue;
                }
                var line = parse.Parse(input.ReadLine()).ToList();
                if (newProductsIndex != -1)
                {
                    var products = line[newProductsIndex].Trim('[', ']').Split(',').Select(s => int.Parse(s));
                    answersList.Add(products.ToArray()); 
                    numberOfNewProducts = products.Count();
                    var newProductsRows = new int[productNames().Length];
                    foreach (var ind in products)
                    {
                        newProductsRows[ind] = 1;
                    }
                    line.AddRange(newProductsRows.Select(p => p.ToString()));
                }

                var dataRow = new List<string>();
                for (int ind=0; ind<line.Count; ind++)
                {
                    if (!removeColumnIndices.Contains(ind))
                    {
                        dataRow.Add(line[ind]);
                    }
                }
                file.WriteLine(CsvUtility.ToCsvLine(dataRow));
            }
          answers = answersList.ToArray();
            input.Close();
            file.Close();
        }

        public static string ToCsvLine(IList<string> fields)
        {
            var line = fields.Select(s => s.Contains(",") ? String.Format("\"{0}\"", s) : s).ToList();
            return string.Join(",", line);
        }

      public static string ToCsvField(string field)
      {
        if (field.Contains(','))
        {
          field = '"' + field + '"';
        }
        return field.Contains(',') ? String.Format("\"{0}\"",field) : field;
      }


        public static string[] productNames()
        {
            return new[] { 
                "ind_ahor_fin_ult1",
                "ind_aval_fin_ult1",
                "ind_cco_fin_ult1",
                "ind_cder_fin_ult1",
                "ind_cno_fin_ult1",
                "ind_ctju_fin_ult1",
                "ind_ctma_fin_ult1",
                "ind_ctop_fin_ult1",
                "ind_ctpp_fin_ult1",
                "ind_deco_fin_ult1",
                "ind_deme_fin_ult1",
                "ind_dela_fin_ult1",
                "ind_ecue_fin_ult1",
                "ind_fond_fin_ult1",
                "ind_hip_fin_ult1",
                "ind_plan_fin_ult1",
                "ind_pres_fin_ult1",
                "ind_reca_fin_ult1",
                "ind_tjcr_fin_ult1",
                "ind_valo_fin_ult1",
                "ind_viv_fin_ult1",
                "ind_nomina_ult1",
                "ind_nom_pens_ult1",
                "ind_recibo_ult1"
            };
        }
    }
}
