﻿using System.IO;
using System.Linq;
using System.Text;
using Encog.App.Analyst;
using Encog.App.Analyst.Util;
using Encog.Engine.Network.Activation;
using Encog.ML;
using Encog.ML.Data.Basic;
using Encog.ML.Train;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation.Back;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using Encog.Util.CSV;
using Encog.Util.Simple;

namespace ProductRecommendationCore
{
  public class CustomMlpBuilder : NeuralNetwork
  {
    private double _classificationTreshold = 0.2;
    private BasicNetwork _network;
    private bool _bias;
    private int[] _neuronCounts;
    private IActivationFunction _activationFunction;
    public CustomMlpBuilder(string fileName, bool bias, int[] neuronCounts, IActivationFunction activationFunction)
    {
      _bias = bias;
      _neuronCounts = neuronCounts;
      _activationFunction = activationFunction;

      ReadData(fileName);
      _normalizationHelper = _dataSet.NormHelper;

      CreateNetwork();
    }

    private void CreateNetwork()
    {
      _network = new BasicNetwork();

      _network.AddLayer(new BasicLayer(null, _bias, _dataSet.CalculatedInputSize));
      foreach (var count in _neuronCounts)
      {
        _network.AddLayer(new BasicLayer(_activationFunction, _bias, count));
      }
      _network.AddLayer(new BasicLayer(_activationFunction, false, _outputColumnsCount));
      _network.Structure.FinalizeStructure();
    }

    public double Train(out int iterations)
    {
      IMLTrain train = new ResilientPropagation(_network, _dataSet, 0.2, 0.3);
      var epoch = 1;
      do
      {
        train.Iteration();
        epoch++;
      } while (epoch < 100 && train.Error > 0.00001);
      train.FinishTraining();

      iterations = epoch;
      return train.Error;
    }

    public double Test()
    {
      var testError = _network.CalculateError(_model.ValidationDataset);
      return testError;
    }

    public void ComputeResults(string fileName, string outputFile, bool submission = false)
    {
      System.Console.WriteLine("Computing results...");

      var csvFile = new ReadCSV(fileName, true, CSVFormat.DecimalPoint);
      var resultCsvData = new StringBuilder();
      if (submission)
      {
        resultCsvData.AppendLine("ncodpers,added_products");
      }
      var input = _normalizationHelper.AllocateInputVector();
      while (csvFile.Next())
      {
        if (submission)
        {
          resultCsvData.Append(csvFile.Get(0)).Append(','); // pierwsza kolumna w pliku testowym to id klienta                
        }

        var dataLine = new string[csvFile.ColumnCount - 1];
        for (int i = 0; i < dataLine.Length; i++)
        {
          dataLine[i] = csvFile.Get(i + 1);
        }
        if (dataLine[15] == "NA")
        {
          dataLine[15] = "0.0";
        }
        _normalizationHelper.NormalizeInputVector(dataLine, ((BasicMLData)input).Data, false);
        var outputClassIndex = _network.Compute(input);
        if (!submission)
        {
          resultCsvData.Append(CsvUtility.ToCsvLine(dataLine)).Append(',');
        }
        var outputClass = _normalizationHelper.DenormalizeOutputVectorToString(outputClassIndex);
        var afterTreshold = outputClass.Select(s => double.Parse(s) > _classificationTreshold).ToList();

        if (!submission)
        {
          resultCsvData.AppendLine(CsvUtility.ToCsvLine(afterTreshold.Select(b => b ? "1" : "0").ToArray()));
        }
        else
        {
          for (var i = 0; i < afterTreshold.Count; i++)
          {
            if (afterTreshold[i])
            {
              resultCsvData.Append(products[i]).Append(" ");
            }
          }
          resultCsvData.AppendLine();
        }
      }

      File.WriteAllText(outputFile, resultCsvData.ToString());
    }
  }
}
