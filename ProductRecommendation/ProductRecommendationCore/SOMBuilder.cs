﻿using Encog.MathUtil.Matrices;
using Encog.MathUtil.RBF;
using Encog.ML.Data;
using Encog.ML.Data.Basic;
using Encog.ML.Factory;
using Encog.Neural.SOM;
using Encog.Neural.SOM.Training.Neighborhood;
using Encog.Util.CSV;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductRecommendationCore
{
    public class SOMBuilder : NeuralNetwork
    {
        private SOMNetwork network;
        private NeighborhoodRBF neighborhoodFunction;
        private int[][] answers;
        private Dictionary<int, List<int>> classesForOutputNeuron;
      private int _maxIterations;

        public SOMBuilder(string filename, int[][] _answers, int maxIterations)
        {
            ReadData(filename);
            answers = _answers;
          _maxIterations = maxIterations;
          _normalizationHelper = _dataSet.NormHelper;

            network = new SOMNetwork(_dataSet.CalculatedInputSize, 24);
            neighborhoodFunction = new NeighborhoodRBF(RBFEnum.Gaussian, 6, 4);
        }

        public double Train(double decayMultiplier)
        {
            var train = new BasicTrainSOM(network, 0.01, _dataSet, neighborhoodFunction);
            train.ForceWinner = false;
            train.SetAutoDecay(_maxIterations, decayMultiplier * 0.8, 0.01, decayMultiplier * 4, 2);

            int iterations = 1;
            while (iterations < _maxIterations)
            {
                train.Iteration();
                train.AutoDecay();
                iterations++;
              if (iterations%10 == 0)
              {
                System.Console.WriteLine("iterations:\t\t\t{0}", iterations);
              }
            }
            train.FinishTraining();

            MatchOutputNeuronsToClasses();

            return train.Error;
        }

        private void MatchOutputNeuronsToClasses()
        {
            var classesCountsForOutputNeuron = new Dictionary<int, Dictionary<int, int>>();
            for (int i = 0; i < _outputColumnsCount; i++)
            {
                classesCountsForOutputNeuron.Add(i, new Dictionary<int, int>());
            }

            for (int i=0; i<_dataSet.Data.Length; i++)
            {
                var winner = network.Winner(new BasicMLData(_dataSet.Data[i].Take(_dataSet.CalculatedInputSize).ToArray()));
                foreach (var answer in answers[i])
                {
                    if (classesCountsForOutputNeuron[winner].ContainsKey(answer))
                    {
                        classesCountsForOutputNeuron[winner][answer]++;
                    }
                    else
                    {
                        classesCountsForOutputNeuron[winner].Add(answer, 1);
                    }
                }
            }

            classesForOutputNeuron = new Dictionary<int, List<int>>();
            for (int i = 0; i < _outputColumnsCount; i++)
            {
                classesForOutputNeuron[i] = classesCountsForOutputNeuron[i].OrderByDescending(c => c.Value).Take(3).Select(c => c.Key).ToList();
            }
        }

        public void ComputeResults(string filename, string outputfile, bool submission = false)
        {
          Console.WriteLine("Computing results...");
            var csvFile = new ReadCSV(filename, true, CSVFormat.DecimalPoint);
            var resultCsvData = new StringBuilder();
            var input = _normalizationHelper.AllocateInputVector();
          var rows = 0;
            while (csvFile.Next())
            {
                resultCsvData.Append(csvFile.Get(0)).Append(','); // pierwsza kolumna w pliku testowym to id klienta
                var dataLine = new string[csvFile.ColumnCount-1];
              rows++;
              //if(rows % 100 == 0)
              //Console.WriteLine("iteration: {0}", rows);
              for (int i = 0; i < dataLine.Length; i++)
                {
                    var data = csvFile.Get(i+1);
                    if(!submission) resultCsvData.Append(CsvUtility.ToCsvField(data)).Append(',');
                    dataLine[i] = data;
                }
              if (dataLine[15] == "NA")
              {
                dataLine[15] = "0.0";
              }
                _normalizationHelper.NormalizeInputVector(dataLine, ((BasicMLData)input).Data, false);
              

                // wersja: odpowiedzią jest jeden neuron
                //var outputClass = network.Winner(input);
                //resultCsvData.AppendLine("," + CsvUtility.ToCsvLine(classesForOutputNeuron[outputClass].Select(c => c.ToString()).ToList()));
                // 

                // wersja: odpowiedzią jest kilka najbardziej prawdopodobnych neuronów
                var output = ComputeOutput(network, (BasicMLData)input);
                var outputResults = _normalizationHelper.DenormalizeOutputVectorToString(output);
                var classificationTreshold = double.Parse(outputResults.Max()) - 0.5;
                var classes = new HashSet<int>();
                for (int i=0; i < outputResults.Length; i++)
                {
                    if (double.Parse(outputResults[i]) > classificationTreshold)
                    {
                        foreach (var c in classesForOutputNeuron[i])
                        {
                            classes.Add(c);
                        }
                    }
                }
              resultCsvData.AppendLine(submission
                ? CsvUtility.ToCsvField(string.Join(" ", classes.Select(c => products[c])))
                : CsvUtility.ToCsvLine(classes.Select(c => c.ToString()).ToList()));
              //
            }

            File.WriteAllText(outputfile, resultCsvData.ToString());
        }

        private BasicMLData ComputeOutput(SOMNetwork som, BasicMLData input)
        {
            BasicMLData result = new BasicMLData(som.OutputCount);
            for (int i = 0; i < som.OutputCount; i++)
            {
                Matrix weights = som.Weights.GetRow(i);
                Matrix inputMatrix = Matrix.CreateRowMatrix(input.Data);
                result.Data[i] = MatrixMath.DotProduct(inputMatrix, weights);
            }
            return result;
        }
    }
}
