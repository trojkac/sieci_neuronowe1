﻿using Encog.ML;
using Encog.ML.Data.Basic;
using Encog.Util.CSV;
using Encog.Util.Simple;
using System.IO;
using System.Linq;
using System.Text;
using Encog.App.Analyst;
using Encog.App.Analyst.Util;

namespace ProductRecommendationCore
{
  public class MLPBuilder : NeuralNetwork
  {
    private IMLRegression _bestMethod;
    private double _classificationTreshold = 0.2;
    private int _xvalidate = 30;

    public MLPBuilder(string fileName, int xvalidate)
    {
      _xvalidate = xvalidate;
      ReadData(fileName);
      //_model.SelectMethod(_dataSet, MLMethodFactory.TypeFeedforward);
      //_dataSet.Normalize();
    }

    public double Train(out int iterations)
    {
      var analyst = new EncogAnalyst();
      _model.Report = new AnalystReportBridge(analyst);
      analyst.AddAnalystListener(new ConsoleAnalystListener());
      analyst.ReportTrainingBegin();
      _model.HoldBackValidation(0.2, false, 1234);
      _model.SelectTrainingType(_dataSet);
      _bestMethod = (IMLRegression)_model.Crossvalidate(_xvalidate, false);

      var trainingError = EncogUtility.CalculateRegressionError(_bestMethod, _model.TrainingDataset);
      _normalizationHelper = _dataSet.NormHelper;
      analyst.ReportTrainingEnd();
      iterations = analyst.MaxIteration;
      return trainingError;
    }

    public double Test()
    {
      var testError = EncogUtility.CalculateRegressionError(_bestMethod, _model.ValidationDataset);
      return testError;
    }

    public void ComputeResults(string fileName, string outputFile, bool submission = false)
    {
      System.Console.WriteLine("Computing results...");

      var csvFile = new ReadCSV(fileName, true, CSVFormat.DecimalPoint);
      var resultCsvData = new StringBuilder();
      if (submission)
      {
        resultCsvData.AppendLine("ncodpers,added_products");
      }
      var input = _normalizationHelper.AllocateInputVector();
      while (csvFile.Next())
      {
        
          resultCsvData.Append(csvFile.Get(0)).Append(','); // pierwsza kolumna w pliku testowym to id klienta                
        
        var dataLine = new string[csvFile.ColumnCount - 1];
        for (int i = 0; i < dataLine.Length; i++)
        {
          dataLine[i] = csvFile.Get(i + 1);
        }
        if (dataLine[15] == "NA")
        {
          dataLine[15] = "0.0";
        }
        _normalizationHelper.NormalizeInputVector(dataLine, ((BasicMLData)input).Data, false);
        var outputClassIndex = _bestMethod.Compute(input);
        if (!submission)
        {
          resultCsvData.Append(CsvUtility.ToCsvLine(dataLine)).Append(',');
        }
        var outputClass = _normalizationHelper.DenormalizeOutputVectorToString(outputClassIndex);
        var afterTreshold = outputClass.Select(s => double.Parse(s) > _classificationTreshold).ToList();

        if (!submission)
        {
          resultCsvData.AppendLine(CsvUtility.ToCsvLine(afterTreshold.Select(b => b ? "1" : "0").ToArray()));
        }
        else
        {
          for (var i = 0; i < afterTreshold.Count; i++)
          {
            if (afterTreshold[i])
            {
              resultCsvData.Append(products[i]).Append(' ');
            }
          }
          resultCsvData.AppendLine();
        }
      }

      File.WriteAllText(outputFile, resultCsvData.ToString());
    }
  }
}
