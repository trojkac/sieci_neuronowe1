function drawPlotRegression(filename, color)

    if nargin < 2
       color = 'red';
    end
    data = csvread(filename,1,0);
    
    hold on
    x = data(:,1);
    y = data(:,2);
    plot(x, y, '.', 'MarkerSize', 6, 'color', color);
    hold off

end