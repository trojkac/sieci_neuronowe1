function drawPlotClassification(filename, trainFile)
    colors = [[1 0 0]; [0 1 0]; [0 0 1]; [1 1 0]; [1 0 1]; [0 1 1]];
    trainColors = [[.8 .3 .3]; 
        [.3 .8 .3]; 
        [.3 .3 .8]; 
        [.8 .8 .3];
        [.8 .3 .8]; 
        [.3 .8 .8]];
    
    drawFile(filename, colors)
    
    if nargin > 1
        drawFile(trainFile, trainColors)
    end
    
end


function drawFile(filename, colors)
data = csvread(filename,1,0);
    classes = unique(data(:,3));
    
    hold on
    for i=1:size(classes,1)
        rowsIndices = data(:,3) == classes(i);
        rows = data(rowsIndices,:);
        x = rows(:,1);
        y = rows(:,2);
        s=scatter(x, y, 12, colors(i,:));
        alpha(.5)
    end
    hold off

end
