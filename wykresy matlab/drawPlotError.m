function drawPlotError(filename, color)

    if nargin < 2
       color = 'black';
    end
    data = csvread(filename,1,0);
    
    hold on
    x = data(:,1);
    y = data(:,2);
    line(x, y, 'color', color);
    hold off

end