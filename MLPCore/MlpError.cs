namespace MLPCore
{
    public class MlpError
    {
        public double TrainingError { get; set; }
        public double TestError { get; set; }
    }
}