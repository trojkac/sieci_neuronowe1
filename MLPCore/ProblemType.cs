﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLPCore
{
    public enum ProblemType
    {
        Classification,
        Regression
    }
}
