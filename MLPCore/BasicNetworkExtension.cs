﻿using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using System.Collections.Generic;

namespace MLPCore
{
    public static class BasicNetworkExtension
    {
        public static void AddLayers(this BasicNetwork network, IEnumerable<ILayer> layers)
        {
            foreach (var layer in layers)
            {
                network.AddLayer(layer);
            }
        }
    }
}
