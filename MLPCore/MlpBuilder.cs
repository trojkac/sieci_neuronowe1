﻿using Encog.App.Analyst;
using Encog.App.Analyst.CSV.Normalize;
using Encog.App.Analyst.Wizard;
using Encog.ML.Data;
using Encog.ML.Data.Specific;
using Encog.ML.Train;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation.Back;
using Encog.Util.CSV;
using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Encog.Engine.Network.Activation;
using Encog.Util.Simple;
using Encog.Neural.Networks.Training.Propagation.Resilient;

namespace MLPCore
{
    public class MlpBuilder
    {
        private CSVMLDataSet _data;
        private CSVMLDataSet _testData;

        private BasicNetwork _network;
        private EncogAnalyst _analystTraining;
        private EncogAnalyst _analystTest;
        private NormalizeRange _range;
        
        private ProblemType _problem;
        private int _iterations;
        private double _learningRate;
        private double _momentum;

        public MlpBuilder(ProblemType problem, int iterations, double learningRate, double momentum)
        {
            _problem = problem;
            _iterations = iterations;
            _learningRate = learningRate;
            _momentum = momentum;
        }

        public int ReadTrainingData(string inputFile, IActivationFunction activationFunc)
        {
            _analystTraining = new EncogAnalyst();
            _analystTraining.Method = new BasicNetwork();
            _range = activationFunc is ActivationSigmoid ? NormalizeRange.Zero2One : NormalizeRange.NegOne2One;

            NormalizeData(inputFile, "normalizedTrain.csv", _analystTraining, true);

            int inputColumnsCount, outputColumnsCount;
            if (_problem == ProblemType.Classification)
            {
                inputColumnsCount = 2;
                outputColumnsCount = _analystTraining.Script.Normalize.NormalizedFields[2].Classes.Count - 1;
            }
            else
            {
                inputColumnsCount = 1;
                outputColumnsCount = 1;
            }

            _data = new CSVMLDataSet("normalizedTrain.csv", inputColumnsCount, outputColumnsCount, true);

            return outputColumnsCount;
        }

        public void ReadTestData(string inputFile)
        {
            _analystTest = new EncogAnalyst {Method = new BasicNetwork()};
            NormalizeData(inputFile, "normalizedTest.csv", _analystTest, false);
            var inputColumnsCount = _problem == ProblemType.Classification ? 2 : 1;
            _testData = new CSVMLDataSet("normalizedTest.csv", inputColumnsCount, 0, true);
        }

        private void NormalizeData(string inputFile, string outputFile, EncogAnalyst analyst, bool isTraining)
        {
            var sourceFile = new FileInfo(inputFile);
            var targetFile = new FileInfo(outputFile);

            var wizard = new AnalystWizard(analyst)
            {
                Range = _range,
                Goal =
                    (_problem == ProblemType.Classification && isTraining)
                        ? AnalystGoal.Classification
                        : AnalystGoal.Regression
            };

            wizard.Wizard(sourceFile, true, AnalystFileFormat.DecpntComma);

            var norm = new AnalystNormalizeCSV();
            norm.Analyze(sourceFile, true, CSVFormat.DecimalPoint, analyst);
            norm.ProduceOutputHeaders = true;
            norm.Normalize(targetFile);
            analyst.Save(new FileInfo(inputFile+"_normalizeStats.ega"));
        }

        public void SetupNetwork(ILayer[] layers)
        {
            _network = new BasicNetwork();
            _network.AddLayers(layers);
            _network.Structure.FinalizeStructure();
            _network.Reset();
        }

        public async Task<MlpError> Train(IProgress<int> progress = null)
        {
            IMLTrain train = new ResilientPropagation(_network, _data, _learningRate, _momentum);
            var epoch = 1;
            var fileStream = new StreamWriter("errorFile.csv");

            await Task.Factory.StartNew(() =>
            {
                do
                {
                    train.Iteration();
                    epoch++;
                    if (progress != null && epoch % 100 == 0)
                    {
                        progress.Report((epoch * 100 )/ _iterations);
                        fileStream.WriteLineAsync($"{epoch},{train.Error}");
                    }
                } while (epoch < _iterations && train.Error  > 0.00001);
            });
            fileStream.Close();
             train.FinishTraining();
            var error = new MlpError();
            error.TrainingError = train.Error;
            return error;
        }

        public void ComputeTestResults()
        {
            var csvData = new StringBuilder();
            foreach (IMLDataPair pair in _testData)
            {
                IMLData output = _network.Compute(pair.Input);
                if (_problem == ProblemType.Classification)
                {
                    var denormalizedInputX = _analystTest.Script.Normalize.NormalizedFields[0].DeNormalize(pair.Input[0]);
                    var denormalizedInputY = _analystTest.Script.Normalize.NormalizedFields[1].DeNormalize(pair.Input[1]);

                    int classCount = _analystTraining.Script.Normalize.NormalizedFields[2].Classes.Count;
                    double normalizationHigh = _analystTraining.Script.Normalize.NormalizedFields[2].NormalizedHigh;
                    double normalizationLow = _analystTraining.Script.Normalize.NormalizedFields[2].NormalizedLow;
                    var eq = new Encog.MathUtil.Equilateral(classCount, normalizationHigh, normalizationLow);
                    var predictedClassIndex = eq.Decode(output);

                    var denormalizedOutput = _analystTraining.Script.Normalize.NormalizedFields[2].Classes[predictedClassIndex].Name;
                    csvData.AppendLine(string.Format("{0},{1},{2}", denormalizedInputX.ToString(CultureInfo.InvariantCulture),
                        denormalizedInputY.ToString(CultureInfo.InvariantCulture), denormalizedOutput));
                }
                else
                {
                    var denormalizedInput = _analystTest.Script.Normalize.NormalizedFields[0].DeNormalize(pair.Input[0]);
                    var denormalizedOutput = _analystTraining.Script.Normalize.NormalizedFields[1].DeNormalize(output[0]);
                    csvData.AppendLine(string.Format("{0},{1}", denormalizedInput.ToString(CultureInfo.InvariantCulture),
                        denormalizedOutput.ToString(CultureInfo.InvariantCulture)));
                }
            }
            File.WriteAllText("results.csv", csvData.ToString());
        }
    }
}
