﻿using Encog;
using Encog.Engine.Network.Activation;
using Encog.Neural.Networks.Layers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MLPCore;

namespace MLP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Properties

        private string _trainingDataFileFullPath;
        private string trainingDataFile;
        public string TrainingDataFile
        {
            get { return trainingDataFile; }
            set
            {
                trainingDataFile = value;
                NotifyPropertyChanged();
            }
        }

        private string _testDataFullPath;
        private string testDataFile;
        public string TestDataFile
        {
            get { return testDataFile; }
            set
            {
                testDataFile = value;
                NotifyPropertyChanged();
            }
        }

        private string hiddenLayersCount;
        public string HiddenLayersCount
        {
            get { return hiddenLayersCount; }
            set
            {
                hiddenLayersCount = value;
                NotifyPropertyChanged();
                UpdateLayersRows();
            }
        }

        private string learningRate;
        public string LearningRate
        {
            get { return learningRate; }
            set
            {
                learningRate = value;
                NotifyPropertyChanged();
            }
        }

        private string momentum;
        public string Momentum
        {
            get { return momentum; }
            set
            {
                momentum = value;
                NotifyPropertyChanged();
            }
        }

        private string iterationsCount;
        public string IterationsCount
        {
            get { return iterationsCount; }
            set
            {
                iterationsCount = value;
                NotifyPropertyChanged();
            }
        }

        private bool bias;
        private MlpError _errorRate;
        private bool _computationRunning;

        public bool Bias
        {
            get { return bias; }
            set
            {
                bias = value;
                NotifyPropertyChanged();
            }
        }

        public MlpError ErrorRate
        {
            get { return _errorRate; }
            set
            {
                _errorRate = value;
                NotifyPropertyChanged();
            }
        }

        public bool ComputationRunning
        {
            get { return _computationRunning; }
            set
            {
                _computationRunning = value; 
                NotifyPropertyChanged();
            }
        }

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            HiddenLayersCount = "1";
            Bias = true;
            LearningRate = "0.03";
            Momentum = "0";
            IterationsCount = "10000";
        }

        private void LoadTrainingDataButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                TrainingDataFile = openFileDialog.SafeFileName;
                _trainingDataFileFullPath = openFileDialog.FileName;
            }
        }

        private void LoadTestDataButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                TestDataFile = openFileDialog.SafeFileName;
                _testDataFullPath = openFileDialog.FileName;
            }
        }

        void ReportProgress(int value)
        {
            ComputationProgress.Value = value;
        }

        private async void ComputeButton_Click(object sender, RoutedEventArgs e)
        {
            ComputationRunning = true;

            var problem = problemType.SelectedIndex == 0 ? ProblemType.Classification : ProblemType.Regression;
            var builder = new MlpBuilder(problem, int.Parse(IterationsCount), double.Parse(LearningRate, CultureInfo.InvariantCulture), double.Parse(Momentum, CultureInfo.InvariantCulture));
            IActivationFunction activationFunc;
            if (activationFunction.SelectedIndex == 0) // unipolar function
                activationFunc = new ActivationSigmoid();
            else // bipolar function
                activationFunc = new ActivationTANH();

            int outputLayerNeuronsCount = builder.ReadTrainingData(_trainingDataFileFullPath, activationFunc);
            builder.ReadTestData(_testDataFullPath);

            builder.SetupNetwork(CreateLayers(outputLayerNeuronsCount,activationFunc));
            var progressIndicator = new Progress<int>(ReportProgress);
            ComputationProgress.Visibility = Visibility.Visible;
            ErrorRate = await builder.Train(progressIndicator);
            builder.ComputeTestResults();
            EncogFramework.Instance.Shutdown();

            ComputationResult.Visibility = Visibility.Visible;
            ComputationRunning = false;

        }

        private ILayer[] CreateLayers(int outputLayerNeuronsCount, IActivationFunction activationFunc)
        {
            var neuronCounts = GetHiddenLayersNeuronCounts();
            int inputLayerNerounCount = problemType.SelectedIndex == 0 ? 2 : 1; // classification = 2 input neurons, regression = 1 input neuron
            
            
            List<ILayer> layers = new List<ILayer>();
            layers.Add(new BasicLayer(null, Bias, inputLayerNerounCount));
            for (int i=0; i<neuronCounts.Length; i++)
            {
                layers.Add(new BasicLayer(activationFunc, Bias, neuronCounts[i]));
            }
            layers.Add(new BasicLayer(activationFunc, false, outputLayerNeuronsCount));

            return layers.ToArray();
        }

        private void UpdateLayersRows()
        {
            int layersCount;
            if (!int.TryParse(HiddenLayersCount, out layersCount))
            {
                layersCount = 1;
            }

            layersPanel.Children.Clear();
            for (int i=1; i<=layersCount; i++)
            {
                var row = new WrapPanel();
                var label = new Label() { Content = string.Concat("Warstwa ", i), Width = 100 };
                var textbox = new TextBox() { Width = 230, Text = "4" };
                row.Children.Add(label);
                row.Children.Add(textbox);

                layersPanel.Children.Add(row);
            }
        }

        private int[] GetHiddenLayersNeuronCounts()
        {
            var neuronCounts = new int[layersPanel.Children.Count];
            for (int i=0; i< layersPanel.Children.Count; i++)
            {
                var row = layersPanel.Children[i];
                var textbox = (row as WrapPanel).Children[1] as TextBox;
                
                if (!int.TryParse(textbox.Text, out neuronCounts[i]))
                {
                    neuronCounts[i] = 1;
                }
            }
            return neuronCounts;
        }
    }
}
