\documentclass{article}
\usepackage[MeX]{polski}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{algorithm}
\usepackage{url}
\usepackage{algorithmic}
\usepackage{color}
\usepackage[table,xcdraw]{xcolor}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{hyperref}

\linespread{1.2}
\title{Sieci neuronowe \\ \textbf{System rekomendacji produktów finansowych dla klientów banku} \\ Raport końcowy}
\author{Kacper Trojanowski\\
Katarzyna Węgiełek}


\begin{document}
\maketitle
\newpage
\clearpage

\section{Opis problemu}
\subsection{Zadanie}
Tematem projektu jest stworzenie systemu rekomendacji produktów finansowych dla klientów banku. Zadanie pochodzi z konkursu opublikowanego na stronie \url{www.kaggle.com}, którego organizatorem jest \textit{Santander Bank}. Bankowy system rekomendacji dla dużej ilości klientów nie jest w stanie odnaleźć produktów, które mógłby zaproponować. Celem projektu jest przewidywanie, jakie produkty kupią obecni klienci w kolejnym miesiącu na podstawie ich wcześniejszego zachowania oraz wyborów dokonywanych przez podobnych do nich klientów.

\subsection{Zastosowane techniki}
W projekcie badaliśmy dwa podejścia do problemu rekomendacji oparte o sieci neuronowe. Jednym z nich był algorytm wykorzystujący sieć MLP, który jest przykładem uczenia maszynowego z nadzorem. Drugie rozwiązanie korzystało z sieci SOM, będącej rodzajem uczenia nienadzorowanego.

Celem badań było porównanie wyników osiąganych przez obie stworzone przez nas sieci oraz określenie, która z metod lepiej radzi sobie ze znalezieniem rekomendacji dla dużej grupy klientów.

\subsection{Technologia}
Projekt został wykonany w języku C\#, w środowisku Visual Studio 2015. Do implementacji mechanizmów związanych z sieciami neuronowymi wykorzystaliśmy bibliotekę \textit{Encog}.

\section{Sposób rozwiązania}
\subsection{Sieć MLP}
Pierwszym zastosowanym w projekcie rodzajem sieci neuronowej był wielowarstwowy perceptron. Jest to sieć typu feed-forward, uczona metodą propagacji wstecznej. Funkcja aktywacji ma postać bipolarną. Na wejście sieci przekazywany jest wektor cech, opisujących danego klienta. Są to informacje o wieku, kraju zamieszkania czy dochodzie oraz lista produktów, które posiada w danym miesiącu. Wyjściem sieci jest lista produktów, które bank powinien zaproponować klientowi w kolejnym miesiącu. Informacja ta jest zakodowana w postaci 24-elementowego wektora, gdzie 1 na i-tym miejscu oznacza, że produkt i został wybrany dla tego klienta.

\subsection{Sieć SOM}
Drugą propozycją rozwiązania problemu rekomendacji jest zastosowanie sieci Kohonena, zwanej samoorganizującą się mapą. Taka sieć składa się z dwóch warstw. Do warstwy wejściowej, podobnie jak w przypadku sieci MLP, przekazywany jest wektor cech klienta wraz z listą posiadanych produktów. Warstwa wyjściowa składa się z 24 neuronów. Każdy neuron odpowiada jednemu produktowi. Po podaniu danych na wejście sieci obliczana jest wartość dla wszystkich neuronów wyjściowej warstwy i jako odpowiedź wybierane jest kilka neuronów o najwyższym wyniku.

\subsection{Opis danych}
Przez bank dostarczone zostały dane klientów zebrane od 28.01.2015r. do 28.05.2016r. Zawierają podstawowe informacje o klientach oraz zakupionych przez nich produktach w okresie jednego miesiąca.

Pojedynczy rekord dotyczący klienta zawiera m.in.:
\begin{itemize}
\item identyfikator klienta
\item wiek
\item płeć
\item kraj zamieszkania
\item region geograficzny
\item jak długo jest klientem banku
\item czy jest aktywnym czy byłym klientem
\item dochód brutto
\item segment klientów
\item czy jest obecnym lub byłym pracownikiem banku lub małżonkiem pracownika
\item czy jest głównym właścicielem konta
\item sposób dołączenia do banku
\item lista zakupionych produktów banku, m.in.:
	\begin{itemize}
	\item konto oszczędnościowe
	\item konto walutowe
	\item konto osobiste
	\item karta kredytowa
	\item polecenie zapłaty
	\item pożyczka
	\item depozyt
	\item gwarancje bankowe
	\end{itemize}
\end{itemize}

\subsection{Wstępne przetworzenie danych}
Otrzymane dane należało najpierw przetworzyć do formy, która mogłaby być podana na wejście sieci neuronowych.
\begin{enumerate}
\item Usunięcie nieznaczących rekordów\\
Dane udostępnione w konkursie zawierały około 13600000 rekordów opisujących klientów. Jednak część z nich zawierała dane niekompletne - nie wszystkie informacje o kliencie były bankowi znane. Ponadto na dane dotyczące każdego kolejnego miesiąca składały się wiersze opisujące wszystkich klientów, byłych i obecnych, niezależnie od tego czy w danym okresie kupili jakiś produkt czy nie. Takich rekordów, gdzie klient nie kupił żadnego produktu nie mogliśmy zastosować jako dane treningowe dla implementowanych sieci, ponieważ nie wnoszą żadnych informacji, których sieć mogłaby się nauczyć. Dlatego ze zbioru danych usunięte zostały wiersze, w których lista posiadanych produktów nie zmieniała się w kolejnym miesiącu oraz te, w których brakowało większości informacji o kliencie. Ostatecznie zostało około 1050000 rekordów.
\item Ignorowanie kolumn nieprzydatnych do uczenia sieci\\
Zdecydowaliśmy, że nie wszystkie kolumny wśród informacji opisujących klienta powinny być przekazywane do sieci. Niektóre dane miały charakter techniczny - np. identyfikator klienta czy miesiąc, z którego pochodzi rekord. Takie informacje nie powinny znaleźć się w danych podawanych na wejście sieci, bo powodowałyby niepotrzebne rozróżnienie klientów i problemy z klasyfikacją. Poza tym niektóre kolumny zawierały dane redundantne. Przykładowo w wierszu opisującym klienta znajdowała się kolumna z datą jego dołączenia do banku oraz liczba miesięcy, przez które jest klientem banku. Niepotrzebne kolumny są usuwane w trakcie działania programu po wczytaniu danych z pliku, przed rozpoczęciem treningu sieci.
\end{enumerate}

\subsection{Normalizacja}
Dane podawane na wejście zarówno sieci MLP jak i sieci SOM muszą być najpierw znormalizowane - przedstawione jako wektor, którego współrzędne należą do przedziału [-1,1]. Jest to konieczne, aby wszystkie używane do uczenia kolumny mogły mieć taki sam wpływ na klasyfikację.\\
Kolumny występujące w danych wejściowych mogą być typu:
\begin{itemize}
\item ciągłego - wartości są liczbami rzeczywistymi, jak np. dochód klienta
\item nominalnego - wartości są etykietami klas, nie są uporządkowane w żadnej kolejności, jak np. płeć czy segment klientów
\end{itemize}
W danych treningowych większość kolumn jest typu nominalnego. Znacząco wpływa to na zwiększenie wymiaru wektora danych wejściowych po normalizacji, zwłaszcza jeśli w danej kolumnie występuje wiele różnych etykiet klas.

Normalizacja i denormalizacja danych przetwarzanych przez sieci została zaimplementowana z użyciem mechanizmu \textit{Encog Modelu}.

\section{Wyniki}
Poniżej przedstawiamy wyniki dla obu zastosowanych przez nas typów sieci, w zależności od testowanych parametrów danego modelu. Sprawdzamy również, jak przekładają się one na liczbę punktów zdobytych w konkursie, która została obliczona przez aplikację oceniającą zgłoszenia.

\subsection{Sieć MLP}
Sieć MLP badaliśmy na dwa sposoby, poprzez budowę sieci za pomocą modelu Encog oraz ręcznie definiując strukturę sieci oraz współczynniki algorytmu uczącego sieci.

\subsubsection{MLP, walidacja krzyżowa}
W pierwszym przypadku skorzystaliśmy ze strategii sprawdzianu krzyżowego z K-krotną walidacją. Polega to na podziale zbioru danych wejściowych na K części, z których jedna jest zbiorem walidacyjnym, natomiast reszta treningowym. Proces nauki powtarzany jest dla sieci K razy wymieniając zbiory treningowe. Naszym zamiarem było sprawdzenie, jak zmiana parametru K wpłynie na błąd sieci. Uzyskane przez nas wyniki pokazują, że najgorszą jakościowo okazała się wartość K=30, natomiast K=20 i K=50 były porównywalne, przy dużo większej wydajności K=20 (2.5 raza mniej iteracji)

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{crossMLP.png} 
 \caption{wykres przedstawia błąd sieci MLP w zależności od zastosowanej wartości współczynnika K dla walidacji krzyżowej}
\end{figure}


\subsubsection{MLP}
W drugim przypadku, przy zdefiniowanej ręcznie strukturze sieci badaliśmy wpływ parametrów algorytmu uczącego na osiągnięte przez sieć wyniki. Zbudowana przez nas sieć miała strukturę zbliżoną do tej, zbudowanej przez model Encog. Posiadała jedną warstwę ukrytą, stosowała bias oraz te same funkcje aktywacji. 

Spośród przeprowadzonych przez nas testów na współczynnik nauki najlepsze wyniki osiągnęliśmy dla wartości równej 0.2, natomiast wartość momentum równa 0.005 była wyraźnie gorsza od pozostałych przez nas testowanych, które dawały podobne wyniki.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{customMLP.png} 
 \caption{Wykres przedstawia błąd sieci MLP w zależności od stałej nauki}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{mlpMomentum.png} 
 \caption{Wykres przedstawia błąd sieci MLP w zależności od momentum}
\end{figure}


\subsection{Sieć Kohonena}
Dla sieci Kohonena badaliśmy wpływ początkowej wartości współczynnika nauki oraz początkowego promienia sąsiedztwa. Obie wartości maleją w trakcie działania algorytmu do ustalonej przez nas wartości, odpowiednio: 0.01 oraz 1.

W przypadku początkowego współczynnika nauki najlepszy wynik osiągnęliśmy dla wartości równej 0.5. Natomiast optymalna wartość początkowego promienia sąsiedztwa wyniosła 4.


\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{som.png} 
 \caption{Wykres przedstawia błąd sieci SOM w zależności od początkowego współczynnika nauki}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{somRadius.png} 
 \caption{Wykres przedstawia błąd sieci SOM w zależności od początkowej wartości promienia sąsiedztwa}
\end{figure}


\subsection{Porównanie wyników}

Po ustaleniu optymalnych parametrów testowanych przez nas typów sieci, wykonaliśmy 10 prób dla sieci MLP, MLP z krzyżową walidacją, oraz sieci Kohonena. W wyniku tych prób okazało się, że zbudowana przez nas sieć MLP, oraz SOM przy każdym podejściu nie zmieniają wielkości błędu. Wynosił on dla sieci MLP 0.17412, dla sieci Kohonena 0.07155. W przypadku sieci MLP z zastosowaniem walidacji krzyżowej minimalny błąd wyniósł 0.17312, maksymalny 0.17395, przy średniej 0.17363, odchylenie standardowe próby wyniosło 0.00024.

\subsubsection{Zestawienie typów sieci}
Do porównania użytych przez nas typów sieci wykorzystaliśmy najważniejszą miarę sukcesu w przypadku naszego zadania, czyli ewaluację modelu przez aplikację konkursową. Zgodnie z oczekiwaniami, najlepszy wynik osiągnęła sieć Kohonena. Sieci MLP uzyskały podobny wynik z lekkim wskazaniem na sieć MLP konfigurowaną przez nas. W zestawieniu pokazaliśmy również wynik osiągnięty przez zwycięzce konkursu, oraz zgłoszenie przykładowe, będące benchmarkiem dla użytkowników.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{kaggle-wyniki.png} 
 \caption{Zestawienie wyników zastosowanych przez nas modeli w porównaniu do benchmarku, oraz zwycięzcy konkursu}
\end{figure}




\section{Wnioski}
Projekt okazał się być bardzo pouczającym i ciekawym doświadczeniem, na podstawie każdego z etapów wykonania zadania konkursowego mogliśmy wyciągnąć odpowiednią naukę. Zaczynając od samej specyfiki zadania, która okazała się być miejscami problematyczna ze względu na specyficzną dziedzinę problemu, którą jest bankowość. Na charakter projektu znacznie wpłynął również wybór użytej przez nas technologii. Biblioteka Encog posiada szeroki zestaw funkcjonalności, co pozwoliło nam na szybką implementację sieci neuronowych, lecz jest przy tym mało elastyczna i momentami jej zachowanie, może być mało przewidywalne. \\
Wyniki, które osiągnęliśmy są zadowalające, lecz nie brak w nich miejsca na poprawę. Wykorzystaliśmy potencjał sieci neuronowych w systemie rekomendacji, jednakże stosując bardziej zaawansowane metody ekstrakcji cech użytkowników, takich jak porównanie przychodu do obszaru zamieszkania, uwzględnienie odrzuconych produktów, itp. prawdopodobnie udałoby nam się polepszyć osiągnięty wynik. Nasze sieci rekomendują zestaw kilku najbardziej popularnych produktów, co jest zgodne z danymi, które wykorzystaliśmy do ich treningu. 


\section{Wykorzystane materiały}
\begin{itemize}
\item \url{https://www.kaggle.com/c/santander-product-recommendation} - szczegółowy opis zadania na stronie konkursu
\item "Programming Neural Networks with Encog3 in C\#" Jeff Heaton
\item "A Recurrent Neural Network Based Recommendation System" David Zhan Liu, Gurbir Singh
\item "A Self-Organizing Map Based Knowledge Discovery for Music Recommendation Systems" Shankar Vembu, Stephan Baumann
\item "Deep Neural Networks for YouTube Recommendations" Paul Covington, Jay Adams, Emre Sargin
\end{itemize}

\end{document}

